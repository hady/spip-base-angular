myApp.filter('filtreMot',function(){
	return function(input, usermots) {
		if (!input) {
			return;
		}
		var resultat = [];
		input.forEach(function(doc){
			// console.log(doc);
			usermots.forEach(function(mot){
				if (inArray(mot, doc.mots)) {
					resultat.push(doc);
				}
			});
		});
		if (!usermots[0]) {
			return input;
		}else{
			return $.uniqueSort(resultat);
		}
	}
});

myApp.filter('filtreMotExclusion',function(){
	return function(input, usermotsexclusion) {
		if (!input) {
			return;
		}
		var resultat = [];
		input.forEach(function(doc){
			// usermotsexclusion.forEach(function(mot){
				if (inArray(parseInt(usermotsexclusion), doc.mots)) {
					resultat.push(doc);
				}
			// });
		});
		if (!usermotsexclusion[0]) {
			return input;
		}else{
			return $.uniqueSort(resultat);
		}
	}
});

myApp.filter('filtreRegions',function(){
	return function(input, usermotsregion) {

		if (!input) {
			return;
		}
		var resultat = [];
		input.forEach(function(doc){
			if (inArray(parseInt(usermotsregion), doc.mots)) {
				resultat.push(doc);
			}
		});

		if (!usermotsregion[0]) {
			return input;
		}else{
			return resultat;
		}
	}
});

function inArray(haystack,needle) {
	if (haystack instanceof Array) {
		if (haystack.indexOf(parseInt(needle)) != -1){
			return true;
		}
	}else{
		if (needle.indexOf(parseInt(haystack)) != -1){
			return true;
		}
	}
	return false;
}

myApp.filter('startFrom', function () {
	return function (input, start) {
		if (input) {
			start = +start;
			return input.slice(start);
		}
		return [];
	};
});


