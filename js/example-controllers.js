var myApp = angular.module('myApp', ['ngSanitize','checklist-model','ui.bootstrap']);

myApp.controller('MainCtrl', ['$scope', 'Server', 'Config', function ($scope, Server, Config) {

	// Définition des variables
	$scope.documents = [];
	$scope.mots_1 = [];
	$scope.mots_2 = [];
	$scope.mots_3 = [];
	$scope.mots_4 = [];
	$scope.mots_exclusion = [];
	$scope.regions_mots = [];

	$scope.recherche = "";
	$scope.titre_bouton_regions = "Région";

	$scope.spinner = true;
	$scope.chargement = 0;

	$scope.user = {
		mots: [],
		mots_exclusion : []
	};
	$scope.select_regions = {
		mots: []
	};

	$scope.status = {
		isopen: false
	};

	// Configuration des url JSON

	var urlDocs = Config.url_articles;
	var urlObjets = Config.url_objets;
	var urlMots1 = Config.url_mots_1;
	var urlMots2 = Config.url_mots_2;
	var urlMots3 = Config.url_mots_3;
	var urlMots4 = Config.url_mots_4;
	var urlMots4 = Config.url_mots_4;
	var urlMots5 = Config.url_mots_exlusion;
	var urlRegionsMots = Config.url_regions_mots;

	// Récupération des données en JSON

	Server.fetchDetails(urlDocs).then(function mySucces(response) {
		$scope.documents = response.data;
		$scope.chargement += 1;
	});

	Server.fetchDetails(urlObjets).then(function mySucces(response) {
		$scope.objets = response.data;
		$scope.chargement += 1;
	});

	Server.fetchDetails(urlMots1).then(function mySucces(response) {
		$scope.mots_1 = response.data;
	});

	Server.fetchDetails(urlMots2).then(function mySucces(response) {
		$scope.mots_2 = response.data;
	});

	Server.fetchDetails(urlMots3).then(function mySucces(response) {
		$scope.mots_3 = response.data;
	});

	Server.fetchDetails(urlMots4).then(function mySucces(response) {
		$scope.mots_4 = response.data;
	});

	Server.fetchDetails(urlMots5).then(function mySucces(response) {
		$scope.mots_exclusion = response.data;
	});

	Server.fetchDetails(urlRegionsMots).then(function mySucces(response) {
		$scope.regions_mots = response.data;
	});


	// Filtrer les régions
	$scope.changeRegion = function(id,titre) {
		$scope.select_regions = {
			mots: []
		};
		$scope.select_regions.mots.push(id);
		$scope.titre_bouton_regions = titre;
	};

	// Reset des filtres
	$scope.resetFiltres = function() {
		$scope.user = {
			mots: [],
			mots_exclusion: []
		};
		$scope.select_regions = {
			mots: []
		};
		$scope.titre_bouton_regions = "Région";
		$scope.recherche = "";
	};

	// Si on fait une recherche en texte libre on réinitialise les checkbox et régions
	$scope.$watch("recherche", function() {
		if ($scope.recherche) {
			$scope.user = {
				mots: [],
				mots_exclusion: []
			};
			$scope.select_regions = {
				mots: []
			};
			$scope.titre_bouton_regions = "Région";
		}
	});

	$scope.$watch("chargement", function() {
		if ($scope.chargement == 2) {
			$scope.spinner = false;
		}
	});


	$scope.$watch("spinner", function() {
		if (!$scope.spinner) {
			$( ".effect" ).hide();
			setTimeout(function(){
				$( ".effect" ).slideDown(800);
			}, 800);
		}
	});

	// Pagination commun
	$scope.maxSize = 5
	,$scope.itemsPerPage = 10;

	// Pagination Documents
	$scope.currentPage = 1
	,$scope.end = 10;

	// Pagination Rubriques
	$scope.currentPageO = 1
	,$scope.endO = 10;

}]);
