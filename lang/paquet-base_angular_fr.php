<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'base_angular_description' => '',
	'base_angular_nom' => 'Une base AngularJS pour SPIP',
	'base_angular_slogan' => '',
);
